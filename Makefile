usage:
	@echo "Usage"
	@echo "	usage (default)"
	@echo "	make lint"
	@echo "	make test_one PACKAGE=package_name TEST=test_name"
	@echo "	make test_all"
	@echo "	make test_cov"
	@echo "	build_and_test"

lint:
	@golangci-lint linters
	@golangci-lint run ./...

test_one:
	@go test ./packages/$(PACKAGE) -run=$(TEST) -v

test_all:
	@go test -coverprofile=coverage.out ./... -v
	@go tool cover -func=coverage.out

test_cov:
	@go test ./... -coverprofile=coverage.out
	@go tool cover -html=coverage.out -o coverage.html

build_and_test:
	@go build .
	@./waltest
	@rm -rf waltest
