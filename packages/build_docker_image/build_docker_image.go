package builddockerimage

//nolint
import (
	"bytes"
	"fmt"
	"strings"

	"treuzedev/waltest/packages/flags"
	"treuzedev/waltest/packages/logging"
)

func Run(
	flags flags.Flags,
	binary string,
	tag string,
	execCommand ExecCommand,

) error {
	logger := logging.GetLogger(flags.NoLogs)

	logger.Log("Building the Docker container..\n\n")

	buildContextDir := getBuildContextDir(flags.Filepath)

	args := []string{
		"build",
		"--file",
		flags.Filepath,
		"--tag",
		tag,
		buildContextDir,
	}

	cmd := execCommand(binary, args...)

	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	var stdout bytes.Buffer
	cmd.Stdout = &stdout

	if err := cmd.Run(); err != nil {
		logger.Log("Something went wrong with the build step:\n\n")
		logger.Log(stderr.String())

		return fmt.Errorf("something went wrong with the build step: %w", err)
	}

	logger.Log("Build step successful!\n\n")
	logger.Log(stdout.String())

	return nil
}

func getBuildContextDir(file string) (buildContextDir string) {
	split := strings.Split(file, "/")

	if len(split) == 1 {
		return "."
	}

	return strings.Split(file, split[len(split)-1])[0]
}
