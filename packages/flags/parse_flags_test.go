package flags_test

//nolint
import (
	"flag"
	"os"
	"testing"
	"treuzedev/waltest/packages/flags"
)

//nolint:paralleltest
func TestRun(t *testing.T) {
	testcases := getRunTestCases()

	for _, testcase := range testcases {
		testcase := testcase

		t.Run(testcase.testName, func(t *testing.T) {
			flagSetup(testcase.flags)

			r := flags.Get()

			if r != testcase.expected {
				t.Errorf(
					"got '%v', expected '%v'",
					r,
					testcase.expected,
				)
			}
		})
	}
}

type parseFlagsTestCase struct {
	testName string
	flags    []string
	expected flags.Flags
}

//nolint
func getRunTestCases() []parseFlagsTestCase {
	return []parseFlagsTestCase{
		{
			testName: "no passed flags",
			flags:    []string{},
			expected: flags.Flags{Filepath: "Dockerfile", NoLogs: false},
		},
		{
			testName: "filepath long flag",
			flags: []string{
				"--filepath",
				"test.Dockerfile",
			},
			expected: flags.Flags{Filepath: "test.Dockerfile", NoLogs: false},
		},
		{
			testName: "filepath short flag",
			flags: []string{
				"-f",
				"test.Dockerfile",
			},
			expected: flags.Flags{Filepath: "test.Dockerfile", NoLogs: false},
		},
		{
			testName: "filepath long and short flag (assumes value of the long flag)",
			flags: []string{
				"-f",
				"short.Dockerfile",
				"--filepath",
				"long.Dockerfile",
			},
			expected: flags.Flags{Filepath: "long.Dockerfile", NoLogs: false},
		},
		{
			testName: "nologs long flag",
			flags: []string{
				"--nologs",
				"true",
			},
			expected: flags.Flags{Filepath: "Dockerfile", NoLogs: true},
		},
		{
			testName: "nologs short flag",
			flags: []string{
				"-s",
				"true",
			},
			expected: flags.Flags{Filepath: "Dockerfile", NoLogs: true},
		},
		{
			testName: "nologs long and short flag (assumes value of the long flag)",
			flags: []string{
				"-s",
				"false",
				"--nologs",
				"true",
			},
			expected: flags.Flags{Filepath: "Dockerfile", NoLogs: true},
		},
		{
			testName: "all short flags",
			flags: []string{
				"-f",
				"test.Dockerfile",
				"-s",
				"true",
			},
			expected: flags.Flags{Filepath: "test.Dockerfile", NoLogs: true},
		},
		{
			testName: "all long flags",
			flags: []string{
				"--filepath",
				"test.Dockerfile",
				"--nologs",
				"true",
			},
			expected: flags.Flags{Filepath: "test.Dockerfile", NoLogs: true},
		},
		{
			testName: "all long and short flags flags - assumes values of long flags",
			flags: []string{
				"-f",
				"test1.Dockerfile",
				"--filepath",
				"test2.Dockerfile",
				"-s",
				"false",
				"--nologs",
				"true",
			},
			expected: flags.Flags{Filepath: "test2.Dockerfile", NoLogs: true},
		},
	}
}

func flagSetup(flags []string) {
	resetFlags()

	os.Args = append(
		[]string{os.Args[0]},
		flags...,
	)
}

func resetFlags() {
	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.ExitOnError)
}
