package flags

import (
	"flag"
	"fmt"
)

func Get() (flags Flags) {
	setFlagUsage()

	filepath := parseFileFlag()
	noLogs := parseNoLogsFlag()
	version := parseVersionFlag()

	flag.Parse()

	return Flags{
		Filepath: *filepath,
		NoLogs:   *noLogs,
		Version:  *version,
	}
}

func parseFileFlag() (flag *string) {
	var fileFlag string

	parseStringFlag(
		&fileFlag,
		"filepath",
		"f",
		"Dockerfile",
	)

	return &fileFlag
}

func parseNoLogsFlag() (flag *bool) {
	var noLogsFlag bool

	parseBoolFlag(
		&noLogsFlag,
		"nologs",
		"s",
		false,
	)

	return &noLogsFlag
}

func parseVersionFlag() (flag *bool) {
	var versionFlag bool

	parseBoolFlag(
		&versionFlag,
		"version",
		"v",
		false,
	)

	return &versionFlag
}

func parseStringFlag(
	flagValue *string,
	longFlag,
	shortFlag,
	defaultValue string,
) {
	flag.StringVar(
		flagValue,
		longFlag,
		defaultValue,
		"",
	)
	flag.StringVar(
		flagValue,
		shortFlag,
		defaultValue,
		"",
	)
}

func parseBoolFlag(
	flagValue *bool,
	longFlag,
	shortFlag string,
	defaultValue bool,
) {
	flag.BoolVar(
		flagValue,
		longFlag,
		defaultValue,
		"",
	)
	flag.BoolVar(
		flagValue,
		shortFlag,
		defaultValue,
		"",
	)
}

func setFlagUsage() {
	usage := `waltest usage:

	--filepath, -f
		Location of the Dockerfile to unit test in relation to where the binary is ran
		Defaults to 'Dockerfile'
	
	--nologs, -s
		If logs are written to stdout
		Defaults to 'false', ie, logs are printed to stdout

	--version, -v
		Prints version

	--help, -h
		Prints usage
	
`
	//nolint:forbidigo
	flag.Usage = func() {
		fmt.Print(usage)
	}
}
