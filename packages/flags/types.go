package flags

type Flags struct {
	Filepath string
	NoLogs   bool
	Version  bool
}
