package readtestcases

import "fmt"

type EmptyTestCaseFileError struct{}

func (err EmptyTestCaseFileError) Error() string {
	return "test cases file is empty"
}

type NoNameError struct {
	TestNumber int
}

func (err NoNameError) Error() string {
	return fmt.Sprintf("unit test number %v has no name", err.TestNumber)
}

type NoExpectedError struct {
	TestNumber int
}

func (err NoExpectedError) Error() string {
	return fmt.Sprintf("unit test number %v has no expected string", err.TestNumber)
}

type BadTestCaseFileError struct {
	Message string
}

func (err BadTestCaseFileError) Error() string {
	return fmt.Sprintf("test cases file is not in the correct format: %v", err.Message)
}

type ParsingYamlError struct {
	Message string
}

func (err ParsingYamlError) Error() string {
	return fmt.Sprintf("error parsing yaml: %v", err.Message)
}
