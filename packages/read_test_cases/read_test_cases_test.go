package readtestcases_test

//nolint
import (
	"errors"
	"testing"

	readtestcases "treuzedev/waltest/packages/read_test_cases"
)

func TestRun(t *testing.T) {
	t.Parallel()

	testcases := getRunTestCases()

	for _, testcase := range testcases {
		testcase := testcase

		t.Run(testcase.name, func(t *testing.T) {
			t.Parallel()

			var expectedTestfileFilepath string

			mockFileReader := func(filepath string) ([]byte, error) {
				expectedTestfileFilepath = filepath

				return []byte(testcase.fileContents), nil
			}

			testCases, err := readtestcases.Run(testcase.filepath, mockFileReader)

			if expectedTestfileFilepath != testcase.expectedTestfileFilepath {
				t.Errorf(
					"got expectedTestfileFilepath '%v', expected expectedTestfileFilepath '%s'",
					expectedTestfileFilepath,
					testcase.expectedTestfileFilepath,
				)
			}

			if !readtestcases.CompareTestCases(testCases, testcase.expectedTestCases) {
				t.Errorf(
					"got fileContents '%v', expected fileContents '%s'",
					testCases,
					testcase.expectedTestCases,
				)
			}

			if !errors.Is(err, testcase.expectedErr) {
				t.Errorf(
					"got error %v, expected error %v",
					err,
					testcase.expectedErr,
				)
			}
		})
	}
}

type readTestCaseTestCase struct {
	name                     string
	filepath                 string
	fileContents             string
	expectedTestfileFilepath string
	expectedTestCases        readtestcases.TestCases
	expectedErr              error
}

//nolint
func getRunTestCases() []readTestCaseTestCase {
	return []readTestCaseTestCase{
		{
			name:                     "handles file with test case with no name - program exits",
			filepath:                 "Dockerfile",
			fileContents:             getFileWithTestCaseNoName(),
			expectedTestfileFilepath: "dockerfile_test.yaml",
			expectedTestCases:        readtestcases.TestCases{},
			expectedErr:              readtestcases.NoNameError{},
		},
		{
			name:                     "handles file with test case with no exected string - program exits",
			filepath:                 "Dockerfile",
			fileContents:             getFileWithTestCaseNoExpectedString(),
			expectedTestfileFilepath: "dockerfile_test.yaml",
			expectedTestCases:        readtestcases.TestCases{},
			expectedErr:              readtestcases.NoExpectedError{},
		},
		{
			name:                     "handles empty file - program exits",
			filepath:                 "Dockerfile",
			fileContents:             getEmptyFile(),
			expectedTestfileFilepath: "dockerfile_test.yaml",
			expectedTestCases:        readtestcases.TestCases{},
			expectedErr:              readtestcases.EmptyTestCaseFileError{},
		},
		{
			name:                     "handles one test case",
			filepath:                 "Dockerfile",
			fileContents:             getFileOneTestCase(),
			expectedTestfileFilepath: "dockerfile_test.yaml",
			expectedTestCases: readtestcases.TestCases{
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
			},
			expectedErr: nil,
		},
		{
			name:                     "handles one test case with multiple inputs in each key",
			filepath:                 "Dockerfile",
			fileContents:             getFileOneTestCaseMultipleAll(),
			expectedTestfileFilepath: "dockerfile_test.yaml",
			expectedTestCases: readtestcases.TestCases{
				{
					Name:     "My test case",
					Options:  "-d -it",
					Command:  "/bin/bash -c",
					Args:     "ls -la",
					Expected: "list of files",
				},
			},
			expectedErr: nil,
		},
		{
			name:                     "handles ten test cases",
			filepath:                 "Dockerfile",
			fileContents:             getFileTenTestCases(),
			expectedTestfileFilepath: "dockerfile_test.yaml",
			expectedTestCases: readtestcases.TestCases{
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
			},
			expectedErr: nil,
		},
		{
			name:                     "handles wrong file structure",
			filepath:                 "Dockerfile",
			fileContents:             getBadFile(),
			expectedTestfileFilepath: "dockerfile_test.yaml",
			expectedTestCases:        readtestcases.TestCases{},
			expectedErr: readtestcases.BadTestCaseFileError{
				Message: `error parsing yaml: yaml: unmarshal errors:
  line 2: cannot unmarshal !!map into readtestcases.TestCases`,
			},
		},
		{
			name:                     "correctly formats file name Dockerfile.dev",
			filepath:                 "Dockerfile.dev",
			fileContents:             getFileOneTestCase(),
			expectedTestfileFilepath: "dockerfile_dev_test.yaml",
			expectedTestCases: readtestcases.TestCases{
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
			},
			expectedErr: nil,
		},
		{
			name:                     "correctly formats file name Dockerfile.dev.MyCompany",
			filepath:                 "Dockerfile.dev.MyCompany",
			fileContents:             getFileOneTestCase(),
			expectedTestfileFilepath: "dockerfile_dev_mycompany_test.yaml",
			expectedTestCases: readtestcases.TestCases{
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
			},
			expectedErr: nil,
		},
		{
			name:                     "correctly formats file name somefolder/Dockerfile.dev.MyCompany",
			filepath:                 "somefolder/Dockerfile.dev.MyCompany",
			fileContents:             getFileOneTestCase(),
			expectedTestfileFilepath: "somefolder/dockerfile_dev_mycompany_test.yaml",
			expectedTestCases: readtestcases.TestCases{
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
			},
			expectedErr: nil,
		},
		{
			name:                     "correctly formats file name some.Folder/Dockerfile.dev.MyCompany",
			filepath:                 "some.Folder/Dockerfile.dev.MyCompany",
			fileContents:             getFileOneTestCase(),
			expectedTestfileFilepath: "some.Folder/dockerfile_dev_mycompany_test.yaml",
			expectedTestCases: readtestcases.TestCases{
				{
					Name:     "My test case",
					Options:  "-d",
					Command:  "/bin/bash",
					Args:     "ls",
					Expected: "list of files",
				},
			},
			expectedErr: nil,
		},
	}
}

func getEmptyFile() (fileContents string) {
	return ""
}

func getFileWithTestCaseNoName() (fileContents string) {
	return `---
- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
`
}

func getFileWithTestCaseNoExpectedString() (fileContents string) {
	return `---
- name: im going to fail
  options: -d
  command: /bin/bash
  args: ls
`
}

func getFileOneTestCase() (fileContents string) {
	return `---
- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case
`
}

func getFileOneTestCaseMultipleAll() (fileContents string) {
	return `---
- options: -d -it
  command: /bin/bash -c
  args: ls -la
  expected: list of files
  name: My test case
`
}

//nolint:funlen
func getFileTenTestCases() (fileContents string) {
	return `---
- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case

- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case

- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case

- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case

- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case

- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case

- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case

- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case

- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case

- options: -d
  command: /bin/bash
  args: ls
  expected: list of files
  name: My test case
`
}

func getBadFile() (fileContents string) {
	return `---
imabadfile: andimabadstring
`
}
