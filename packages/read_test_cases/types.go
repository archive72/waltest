package readtestcases

import "gopkg.in/yaml.v2"

type Reader func(filename string) ([]byte, error)

type TestCase struct {
	Name     string
	Options  string
	Command  string
	Args     string
	Expected string
}

type TestCases []TestCase

func (testCases *TestCases) Parse(data []byte) error {
	err := yaml.Unmarshal(data, testCases)
	if err != nil {
		return ParsingYamlError{Message: err.Error()}
	}

	return nil
}

func CompareTestCases(testCases1, testCases2 TestCases) bool {
	if len(testCases1) != len(testCases2) {
		return false
	}

	for i := 0; i < len(testCases1); i++ {
		if testCases1[i] != testCases2[i] {
			return false
		}
	}

	return true
}
