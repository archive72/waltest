package runtestcases

//nolint
import (
	"bytes"
	"fmt"
	"strings"

	"treuzedev/waltest/packages/helpers"
	"treuzedev/waltest/packages/logging"
	readtestcases "treuzedev/waltest/packages/read_test_cases"
)

func Run(
	testCases readtestcases.TestCases,
	binary string,
	tag string,
	execCommand ExecCommand,
	noLogs bool,
) error {
	logger := logging.GetLogger(noLogs)
	logger.Log("\nRunning test cases..\n\n")

	registerRunOutputs := RegisterRunOutputs{
		Values: []string{},
	}

	for i, testCase := range testCases {
		logger.Log(
			fmt.Sprintf(
				"Running test case '%v' - '%v'\n",
				i,
				testCase.Name,
			),
		)

		args := generateArgs(
			testCase.Options,
			tag,
			testCase.Command,
			testCase.Args,
		)

		rCmd := runDockerCommand(binary, args, execCommand)

		registerRunOutputs.Values = append(registerRunOutputs.Values, rCmd)
	}

	logger.Log("\nSummary:\n\n")

	return checkTestCases(
		testCases,
		registerRunOutputs,
		logger,
	)
}

func runDockerCommand(
	binary string,
	args []string,
	execCommand ExecCommand,
) (rCmd string) {
	cmd := execCommand(binary, args...)

	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	var stdout bytes.Buffer
	cmd.Stdout = &stdout

	if err := cmd.Run(); err != nil {
		return stderr.String()
	}

	return stdout.String()
}

func checkTestCases(
	testCases readtestcases.TestCases,
	registerRunOutputs RegisterRunOutputs,
	logger *logging.WaltestLogger,
) error {
	anyTestFail := false

	for index, testCase := range testCases {
		if testCase.Expected != registerRunOutputs.Values[index] {
			anyTestFail = true

			logger.Log(
				fmt.Sprintf(
					"Test case '%v' - '%v' failed:\n",
					index,
					testCase.Name,
				),
			)
			logger.Log(
				fmt.Sprintf(
					"    Got '%v', expected '%v'\n\n",
					registerRunOutputs.Values[index],
					testCase.Expected,
				),
			)
		} else {
			logger.Log(
				fmt.Sprintf(
					"Test case '%v' - '%v' successful!\n\n",
					index,
					testCase.Name,
				),
			)
		}
	}

	if anyTestFail {
		return AnyTestFailError{}
	}

	return nil
}

func generateArgs(options, tag, command, dockerCommandArgs string) []string {
	optionsAsSlice := getStringAsSlice(options)
	commandAsSlice := getStringAsSlice(command)
	dockerCommandArgsAsSlice := getStringAsSlice(dockerCommandArgs)

	args := []string{"run"}

	emptyStringSlice := []string{}
	testOptionAsEmptyString := []string{""}

	if !helpers.CompareStringSlices(optionsAsSlice, emptyStringSlice) &&
		!helpers.CompareStringSlices(optionsAsSlice, testOptionAsEmptyString) {
		args = append(args, optionsAsSlice...)
	}

	args = append(args, tag)

	if !helpers.CompareStringSlices(commandAsSlice, emptyStringSlice) &&
		!helpers.CompareStringSlices(commandAsSlice, testOptionAsEmptyString) {
		args = append(args, commandAsSlice...)
	}

	if !helpers.CompareStringSlices(dockerCommandArgsAsSlice, emptyStringSlice) &&
		!helpers.CompareStringSlices(dockerCommandArgsAsSlice, testOptionAsEmptyString) {
		args = append(args, dockerCommandArgsAsSlice...)
	}

	return args
}

func getStringAsSlice(str string) []string {
	sliceWithWhitespace := strings.Split(str, " ")

	sliceWithoutWhitespace := []string{}

	for _, str := range sliceWithWhitespace {
		if str != "" {
			sliceWithoutWhitespace = append(sliceWithoutWhitespace, str)
		}
	}

	return sliceWithoutWhitespace
}
