package runtestcases_test

//nolint
import (
	"os"
	"os/exec"
	"testing"

	"treuzedev/waltest/packages/helpers"
	readtestcases "treuzedev/waltest/packages/read_test_cases"
	runtestcases "treuzedev/waltest/packages/run_test_cases"
)

//nolint:funlen
func TestRun(t *testing.T) {
	t.Parallel()

	binary, tag, testcases := getTestVariables()

	t.Run("calls the expected commands on a docker container successfully", func(t *testing.T) {
		t.Parallel()

		calls := 0

		var rName []string
		var rArgs [][]string

		mockExecCommand := func(name string, arg ...string) *exec.Cmd {
			calls++
			rName = append(rName, name)
			rArgs = append(rArgs, arg)

			cs := []string{"-test.run=TestHelperProcess", "--"}
			cmd := exec.Command(os.Args[0], cs...) //nolint:gosec
			cmd.Env = []string{"GO_WANT_HELPER_PROCESS=0"}

			return cmd
		}

		err := runtestcases.Run(
			testcases.testCases,
			binary,
			tag,
			mockExecCommand,
			true,
		)

		checkSuccessfulRunResults(
			t,
			err,
			calls,
			testcases,
			rName,
			rArgs,
			binary,
		)
	})

	t.Run("calls the expected commands on a docker container unsuccessfully", func(t *testing.T) {
		t.Parallel()

		mockErr := runtestcases.AnyTestFailError{}

		mockExecCommand := func(name string, arg ...string) *exec.Cmd {
			cs := []string{"-test.run=TestHelperProcess", "--"}
			cmd := exec.Command(os.Args[0], cs...) //nolint:gosec
			cmd.Env = []string{"GO_WANT_HELPER_PROCESS=1"}

			return cmd
		}

		err := runtestcases.Run(
			testcases.testCases,
			binary,
			tag,
			mockExecCommand,
			true,
		)
		if err.Error() != mockErr.Error() {
			t.Errorf(
				"got error %v, expected error %v",
				err,
				mockErr,
			)
		}
	})
}

type RunTestCasesTestCase struct {
	dockerImage   string
	testCases     []readtestcases.TestCase
	expectedCalls int
	expectedArgs  [][]string
}

func getTestVariables() (string, string, RunTestCasesTestCase) {
	binary := "/bin/docker"
	tag := "waltest:123456789"

	testCases := RunTestCasesTestCase{
		dockerImage: "waltest:123456789",
		testCases: readtestcases.TestCases{
			{
				Options:  "-d",
				Command:  "/bin/bash",
				Args:     "ls",
				Expected: "",
			},
			{
				Options:  "-d",
				Command:  "/bin/bash",
				Args:     "ls",
				Expected: "",
			},
			{
				Options:  " -d    -it",
				Command:  "/bin/bash -c  ",
				Args:     " ls    -la ",
				Expected: "",
			},
		},
		expectedCalls: 3,
		expectedArgs: [][]string{
			{
				"run",
				"-d",
				tag,
				"/bin/bash",
				"ls",
			},
			{
				"run",
				"-d",
				tag,
				"/bin/bash",
				"ls",
			},
			{
				"run",
				"-d",
				"-it",
				tag,
				"/bin/bash",
				"-c",
				"ls",
				"-la",
			},
		},
	}

	return binary, tag, testCases
}

func checkSuccessfulRunResults(
	t *testing.T,
	err error,
	calls int,
	testcases RunTestCasesTestCase,
	rName []string,
	rArgs [][]string,
	binary string,
) {
	t.Helper()

	if err != nil {
		t.Errorf(
			"got %v, expected %v",
			err,
			nil,
		)
	}

	if calls != testcases.expectedCalls {
		t.Errorf(
			"got %v calls, expected %v calls",
			calls,
			testcases.expectedCalls,
		)
	}

	for index := 0; index < testcases.expectedCalls; index++ {
		if rName[index] != binary {
			t.Errorf(
				"got rArgv0[%v] == %v, expected %v",
				index,
				rName[index],
				binary,
			)
		}

		if !helpers.CompareStringSlices(rArgs[index], testcases.expectedArgs[index]) {
			t.Errorf(
				"got rArgv[%v] == %v, expected %v",
				index,
				rArgs[index],
				testcases.expectedArgs[index],
			)
		}
	}
}

func TestHelperProcess(t *testing.T) {
	t.Parallel()

	if os.Getenv("GO_WANT_HELPER_PROCESS") == "0" {
		os.Exit(0)
	}

	if os.Getenv("GO_WANT_HELPER_PROCESS") == "1" {
		return
	}
}
