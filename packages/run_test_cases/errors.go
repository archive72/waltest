package runtestcases

type AnyTestFailError struct{}

func (err AnyTestFailError) Error() string {
	return "a test failed"
}
