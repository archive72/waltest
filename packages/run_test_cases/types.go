package runtestcases

import "os/exec"

type ExecCommand func(name string, arg ...string) *exec.Cmd

type RegisterRunOutputs struct {
	Values []string
}
